# [Snake](http://www.grangle.net/snakeJS)
## Description
[Snake](https://en.wikipedia.org/wiki/Snake_(video_game)) is a classic 2D video game 
where the objective is to control a line as it grows in length while preventing it 
from hitting the bounds or itself.

<div style="text-align:center">
<img src="https://cloud.githubusercontent.com/assets/5321223/25688674/3b39745e-304f-11e7-8499-c2b324333b99.png" height="240">  
<br/>
<br/>
</div>

# How To Play
## Key Controls

- **`w`** or **`↑`** : Move up
- **`s`** or **`↓`** : Move down
- **`a`** or **`←`** : Turn left
- **`d`** or **`→`** : Turn right
- **`+`** : Increasing snake speed
- **`-`** : Decreasing snake speed
- **`r`** : Reset game
- **`p`** : Pause/Resume game
- **`[0...4]`** : Switch to different map. *There are 5 different maps available.*)

# Enhancements

- ## **Fruit**: 
   - ### *There are 6 kinds of fruit available*
      - **Normal** : will extend the snake's tail by one
      - **Shorten** : will chop off the snake's tail by five
      - **Lengthen** : will extend the snake's tail by five
      - **Faster** : will increase the snake's speed by one ( if possible )
      - **Slower** : will decrease the snake's speed by one ( if possible )
      - **Reverse** : will change the snake's current moving direction ( be careful :) )

- ## **Map (Obstacle)**: 
   - ### *There are 5 different maps available*
      - Player can press [0...4] to select which map to play.

- ## **Level**:
   - By default after eating 250 fruits, the game will level up. When level up:
      - Snake's speed will increase by 1. If the speed is currently maximum *(10)*, then no change
      - The map will change after leveling up. 
   - Player can set the number of fruit that requires to level up by specifing the **levelUpRate** 
   when starts the game by running bellow command.
