'use strict';

function BoardView(model) {
  this.model = model;
}

/**
 * Draw block
 */
BoardView.prototype.drawBlock = function (context, x, y, blockSize) {
  var xPos = x * blockSize;
  var yPos = y * blockSize;
  context.beginPath();
  context.rect(xPos, yPos, blockSize, blockSize);
  context.fillStyle = 'black';
  context.fill();
};

/**
 * Draw board
 */
BoardView.prototype.drawBoard = function (context) {
  var row = 0;
  var col = 0;
  var obstacle = this.model.getObstacle();
  var blockSize = this.model.getBlockSize();
  var width = this.model.getWidth();
  var height = this.model.getHeight();
  var maxRow = width / blockSize;
  var maxCol = height / blockSize;
  for (row = 0; row < maxRow; ++row) {
    for (col = 0; col < maxCol; ++col) {
      if (obstacle[row][col]) {
        this.drawBlock(context, row, col, blockSize);
      }
    }
  }
};

/**
 * Draw text
 */
BoardView.prototype.drawText = function (context) {
  context.font = '64px Arcade';
  context.fillStyle = 'darkslategray';
  context.fillText('GAMEOVER', 160, 300);
};

/**
 * Draw view
 */
BoardView.prototype.draw = function () {
  var context = this.model.getContext();
  var isAlive = this.model.getSnakeStatus();
  if (!isAlive) {
    this.drawText(context);
  }
  this.drawBoard(context);
};

/**
 * Update board view
 */
BoardView.prototype.updateView = function () {
  this.draw();
};
