'use strict';

function SnakeView(model) {
  this.model = model;
}

/**
 * Draw block
 */
SnakeView.prototype.drawBlock = function (context, x, y, blockSize) {
  var xPos = x * blockSize;
  var yPos = y * blockSize;
  context.beginPath();
  context.rect(xPos, yPos, blockSize, blockSize);
  context.fillStyle = 'green';
  context.fill();
  context.lineWidth = 1;
  context.strokeStyle = 'white';
  context.stroke();
};

/**
 * Draw snake
 */
SnakeView.prototype.drawSnake = function (context) {
  var snake = this.model.getSnake();
  var blockSize = this.model.getBlockSize();
  var isAlive = this.model.getSnakeStatus();
  if (isAlive) {
    for (snake.front(); snake.currPos() < snake.length(); snake.next()) {
      var block = snake.getElement();
      this.drawBlock(context, block.x, block.y, blockSize);
    }
  }
};

/**
 * Draw view
 */
SnakeView.prototype.draw = function () {
  var context = this.model.getContext();
  this.drawSnake(context);
};

/**
 * Update snake view
 */
SnakeView.prototype.updateView = function () {
  this.draw();
};
