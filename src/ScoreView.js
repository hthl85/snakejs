'use strict';

function ScoreView(model) {
  this.model = model;
}

/**
 * Draw board
 */
ScoreView.prototype.drawScoreBoard = function (context) {
  context.beginPath();
  context.rect(600, 0, 200, 600);
  context.fillStyle = '#64b5f6';
  context.fill();
};

/**
 * Draw score
 */
ScoreView.prototype.drawScore = function (context, score) {
  var str = ('000000' + score).substr(-6, 6);
  context.font = '32px Arcade';
  context.fillStyle = '#d50000';
  context.fillText(str, 645, 85);
};

/**
 * Draw speed
 */
ScoreView.prototype.drawSpeed = function (context, speed) {
  var str = ('Speed : ' + ((1000 / 100) - (speed / 100)));
  context.font = '18px Arcade';
  context.fillStyle = '#ffffff';
  context.fillText(str, 660, 575);
};

/**
 * Draw level
 */
ScoreView.prototype.drawLevel = function (context, level) {
  var str = ('Level : ' + level);
  context.font = '18px Arcade';
  context.fillStyle = '#1b5e20';
  context.fillText(str, 660, 550);
};

/**
 * Draw text
 */
ScoreView.prototype.drawText = function (context) {
  context.font = '36px Arcade';
  context.fillStyle = '#ffff00';
  context.fillText('Snake JS', 630, 50);
};

/**
 * Draw view
 */
ScoreView.prototype.draw = function () {
  var context = this.model.getContext();
  var score = this.model.getScore();
  var speed = this.model.getSpeed();
  var level = this.model.getLevel();
  this.drawScoreBoard(context);
  this.drawText(context);
  this.drawScore(context, score);
  this.drawSpeed(context, speed);
  this.drawLevel(context, level);
};

/**
 * Update fruit view
 */
ScoreView.prototype.updateView = function () {
  this.draw();
};
