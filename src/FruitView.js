'use strict';

function FruitView(model) {
  this.model = model;
}

/**
 * Get fruit color by type
 */
FruitView.prototype.getFruitColor = function (fruitType) {
  var colors = ['gainsboro', 'greenyellow', 'blue', 'blueviolet', 'orangered'];
  if (fruitType >= 0 && fruitType <= 3) {
    return colors[fruitType];
  }
  return colors[colors.length - 1];
};

/**
 * Draw block
 */
FruitView.prototype.drawFruit = function (context, x, y, blockSize, fruitType) {
  var xPos = x * blockSize;
  var yPos = y * blockSize;
  context.beginPath();
  context.rect(xPos, yPos, blockSize, blockSize);
  context.fillStyle = this.getFruitColor(fruitType);
  context.fill();
  context.lineWidth = 1;
  context.strokeStyle = 'white';
  context.stroke();
};

/**
 * Draw view
 */
FruitView.prototype.draw = function () {
  var context = this.model.getContext();
  var fruit = this.model.getFruit();
  var fruitType = this.model.getFruitType();
  var blockSize = this.model.getBlockSize();
  var isAlive = this.model.getSnakeStatus();
  if (isAlive) {
    this.drawFruit(context, fruit.x, fruit.y, blockSize, fruitType);
  }
};

/**
 * Update fruit view
 */
FruitView.prototype.updateView = function () {
  this.draw();
};
