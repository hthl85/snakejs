'use strict';

function SnakeController(model) {
  this.model = model;

  // Add navigation
  this.navigate();
}

/**
 * Snake navigate
 * [0 - up] [1 - down] [2 - left] [3 - right]
 */
SnakeController.prototype.navigate = function () {

  document.addEventListener('keydown', (event) => {
    const keyCode = event.keyCode;

    if (keyCode === 38 || keyCode === 87) { // Up arrow
      // console.log('Up arrow');
      this.model.changeDirection(0);
    } else if (keyCode === 40 || keyCode === 83) { // Down arrow
      // console.log('Down arrow');
      this.model.changeDirection(1);
    } else if (keyCode === 37 || keyCode === 65) { // Left arrow
      // console.log('Left arrow');
      this.model.changeDirection(2);
    } else if (keyCode === 39 || keyCode === 68) { // Right arrow
      // console.log('Right arrow');
      this.model.changeDirection(3);
    } else if (keyCode === 187) { // Increase speed
      // console.log('Increate speed');
      this.model.increaseSpeed();
    } else if (keyCode === 189) { // Decrease speed
      // console.log('Decrease speed');
      this.model.decreaseSpeed();
    } else if (keyCode === 82) { // Reset game
      // console.log('Reset game');
      this.model.reset();
    } else if (keyCode === 80) { // Pause game
      // console.log('Pause game');
      this.model.pause();
    }

  }, false);
}
