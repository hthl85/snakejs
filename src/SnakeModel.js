'use strict';

function Point(x, y) {
  this.x = x;
  this.y = y;
}

function SnakeModel(context, w, h, b) {
  this.views = [];

  this.x = 0;
  this.y = 0;
  this.score = 0;
  this.width = w;
  this.height = h;
  this.speed = 500;
  this.timer = undefined;
  this.blockSize = b;
  this.direction = 0;
  this.obstacle = [];
  this.isAlive = true;
  this.isPause = false;
  this.context = context;
  this.snake = new List();
  this.level = 0;
  this.levelUp = 1000;
  this.fruitType = 10;
  this.fruitScore = 10;
  this.fruit = new Point(25, 25);

  this.initBoard();
}

/**
 * Init the snake body
 */
SnakeModel.prototype.initSnake = function () {
  var idx = 0;
  this.snake.clear();
  for (idx = 0; idx < 3; ++idx) {
    var tail = new Point(5 - idx, 10);
    this.snake.push(tail);
  }
  this.direction = 3;
};

/**
 * Set board border
 */
SnakeModel.prototype.initBoard = function () {
  var row = 0;
  var col = 0;
  var maxRow = this.width / this.blockSize;
  var maxCol = this.height / this.blockSize;
  for (row = 0; row < maxRow; ++row) {
    this.obstacle[row] = [];
    for (col = 0; col < maxCol; ++col) {
      this.obstacle[row][col] = false;
      if (row === 0 || row === maxRow - 1) {
        this.obstacle[row][col] = true;
      }
      if (col === 0 || col === maxCol - 1) {
        this.obstacle[row][col] = true;
      }
    }
  }
};

/**
 * Add view
 */
SnakeModel.prototype.addView = function (view) {
  this.views.push(view);

  // Update the view to current state of the model
  view.updateView();
};

/**
 * Notify observers
 */
SnakeModel.prototype.notifyObservers = function () {
  var idx = 0;
  var len = this.views.length;
  for (idx = 0; idx < len; ++idx) {
    this.views[idx].updateView();
  }
};

/**
 * Get x coordinate
 */
SnakeModel.prototype.getX = function () {
  return this.x;
};

/**
 * Get y coordinate
 */
SnakeModel.prototype.getY = function () {
  return this.y;
};

/**
 * Get score
 */
SnakeModel.prototype.getScore = function () {
  return this.score;
};

/**
 * Get speed
 */
SnakeModel.prototype.getSpeed = function () {
  return this.speed;
};

/**
 * Get level
 */
SnakeModel.prototype.getLevel = function () {
  return this.level;
};

/**
 * Get width
 */
SnakeModel.prototype.getWidth = function () {
  return this.width;
};

/**
 * Get height
 */
SnakeModel.prototype.getHeight = function () {
  return this.height;
};

/**
 * Get snake body
 */
SnakeModel.prototype.getSnake = function () {
  return this.snake;
};

/**
 * Get snake status
 */
SnakeModel.prototype.getSnakeStatus = function () {
  return this.isAlive;
};

/**
 * Get fruit
 */
SnakeModel.prototype.getFruit = function () {
  return this.fruit;
};

/**
 * Get context
 */
SnakeModel.prototype.getContext = function () {
  return this.context;
};

/**
 * Get fruit type
 */
SnakeModel.prototype.getFruitType = function () {
  return this.fruitType;
};

/**
 * Get block size
 */
SnakeModel.prototype.getBlockSize = function () {
  return this.blockSize;
};

/**
 * Get obstacle
 */
SnakeModel.prototype.getObstacle = function () {
  return this.obstacle;
};

/**
 * Change snake's direction
 */
SnakeModel.prototype.changeDirection = function (val) {
  if (val === 0 || val === 1) { // [0 - up] [1 - down]
    if (this.direction != 0 && this.direction != 1) {
      this.direction = val;
    }
  } else if (val === 2 || val === 3) { // [2 - left] [3 - right]
    if (this.direction != 2 && this.direction != 3) {
      this.direction = val;
    }
  }
};

/**
 * Move snake
 */
SnakeModel.prototype.move = function () {
  var newFront = undefined;
  var front = this.snake.getElementAt(0);

  if (this.direction === 0) { // [0 - up]
    newFront = new Point(front.x, front.y - 1);
  } else if (this.direction === 1) { // [1 - down]
    newFront = new Point(front.x, front.y + 1);
  } else if (this.direction === 2) { // [2 - left]
    newFront = new Point(front.x - 1, front.y);
  } else if (this.direction === 3) { // [3 - right]
    newFront = new Point(front.x + 1, front.y);
  }

  this.snake.pushFront(newFront);
  this.snake.pop();

  this.didEatFruit();
  this.didHitWall();
  this.didEatItself();

  this.notifyObservers();
};

/**
 * Generate new fruit
 */
SnakeModel.prototype.generateFruit = function () {
  var randomize = function (min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };

  var comparePoint = function (a, b) {
    return (a.x === b.x && a.y === b.y);
  };

  var isValid = false;
  var maxRow = this.width / this.blockSize;
  var maxCol = this.height / this.blockSize;

  while (!isValid) {
    this.fruitType = randomize(0, 10) - 1;
    var fruitX = randomize(1, maxRow - 1);
    var fruitY = randomize(1, maxCol - 1);
    var newFruit = new Point(fruitX, fruitY);

    var isOnWall = this.obstacle[fruitX][fruitY];
    var isOnSnake = this.snake.contains(newFruit, comparePoint);

    isValid = !isOnWall && !isOnSnake;

    if (isValid) {
      this.fruit = newFruit;
    }
  }
};

/**
 * Handle snake eats fruit event
 */
SnakeModel.prototype.didEatFruit = function () {
  var front = this.snake.getElementAt(0);
  if (this.fruit.x === front.x && this.fruit.y === front.y) {
    // ['gainsboro', 'greenyellow', 'blue', 'blueviolet', 'orangered']
    if (this.fruitType === 0) { // gainsboro
      this.reduceTailByFive();
      this.score += this.fruitScore;
    } else if (this.fruitType === 1) { // greenyellow
      this.decreaseSpeed();
      this.extendTailByOne();
      this.score += this.fruitScore;
    } else if (this.fruitType === 2) { // blue
      this.increaseSpeed();
      this.extendTailByOne();
      this.score += this.fruitScore;
    } else if (this.fruitType === 3) { // blueviolet
      this.extendTailByFive();
      this.score += (this.fruitScore * 5);
    } else { // orangered
      this.extendTailByOne();
      this.score += this.fruitScore;
    }

    if (this.level < Math.floor(this.score / this.levelUp)) {
      this.level += 1;
      this.increaseSpeed();
      this.initSnake();
    }
    this.generateFruit();
  }
};

/**
 * Handle snake hits wall event
 */
SnakeModel.prototype.didHitWall = function () {
  var front = this.snake.getElementAt(0);
  if (this.obstacle[front.x][front.y]) {
    this.isAlive = false;
  }
};

/**
 * Handle snake eats itself event
 */
SnakeModel.prototype.didEatItself = function () {
  var comparePoint = function (a, b) {
    return (a.x === b.x && a.y === b.y);
  };

  this.snake.front();
  this.snake.next();
  var tmp = undefined;
  var front = undefined;
  var isContain = false;

  for (; this.snake.currPos() < this.snake.length(); this.snake.next()) {
    tmp = this.snake.getElement();
    front = this.snake.getElementAt(0);
    isContain = comparePoint(tmp, front);
    if (isContain) {
      this.isAlive = false;
      return;
    }
  }

};

/**
 * Extend snake tail by one
 */
SnakeModel.prototype.extendTailByOne = function () {
  var tail = this.snake.getElementReverseAt(0);
  var newTail = new Point(tail.x, tail.y);
  this.snake.push(newTail);
};

/**
 * Extend snake tail by five
 */
SnakeModel.prototype.extendTailByFive = function () {
  var idx = 0;
  var tail = this.snake.getElementReverseAt(0);
  for (idx = 0; idx < 5; ++idx) {
    var newTail = new Point(tail.x, tail.y);
    this.snake.push(newTail);
  }
};

/**
 * Reduce snake tail by five
 */
SnakeModel.prototype.reduceTailByFive = function () {
  var counter = 0;
  var len = this.snake.length();
  while (len > 3 && counter < 5) {
    this.snake.pop();
    len = this.snake.length();
    counter += 1;
  }
};

/**
 * Increase snake speed
 */
SnakeModel.prototype.increaseSpeed = function () {
  if (this.speed > 100) {
    this.speed -= 100;
    this.notifyObservers();
  }
}

/**
 * Decrease snake speed
 */
SnakeModel.prototype.decreaseSpeed = function () {
  if (this.speed < 1000) {
    this.speed += 100;
    this.notifyObservers();
  }
};

/**
 * Reset game
 */
SnakeModel.prototype.reset = function () {
  this.x = 0;
  this.y = 0;
  this.level = 0;
  this.score = 0;
  this.speed = 500;
  this.direction = 0;
  this.isAlive = true;
  this.isPause = false;
  this.snake = new List();
  this.fruit = new Point(25, 25);

  this.initSnake();

  if (this.timer) {
    clearTimeout(this.timer);
  }
  this.tick();
};

/**
 * Pause game
 */
SnakeModel.prototype.pause = function () {
  this.isPause = !this.isPause;

  if (!this.isPause) {
    if (this.timer) {
      clearTimeout(this.timer);
    }
    this.tick();
  }
};

/**
 * Tick
 */
SnakeModel.prototype.tick = function () {
  var that = this;

  var reDraw = function () {
    if (that.isAlive && !that.isPause) {
      that.context.clearRect(0, 0, that.width, that.height);
      that.move();
      that.timer = setTimeout(reDraw, that.speed);
    }
  };

  reDraw();
};
