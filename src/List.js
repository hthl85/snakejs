'use strict';

function List() {
  this.pos = 0;
  this.listSize = 0;
  this.dataStore = [];
}

/**
 * Insert an element into a list after a given element
 * Return true if success. Otherwise, return false
 */
List.prototype.insert = function (element, after, compareFunc) {
  var insertPos = this.find(after, compareFunc);
  if (insertPos > -1) {
    this.dataStore.splice(insertPos + 1, 0, element);
    this.listSize += 1;
    return true;
  }
  return false;
};

/**
 * Remove an element from a list
 * Return true if success. Otherwise, return false
 */
List.prototype.remove = function (element, compareFunc) {
  var foundAt = this.find(element, compareFunc);
  if (foundAt > -1) {
    this.dataStore.splice(foundAt, 1);
    this.listSize -= 1;
    return true;
  }
  return false;
};

/**
 * Add a new element to the end of a list
 */
List.prototype.push = function (element) {
  this.dataStore[this.listSize] = element;
  this.listSize += 1;
};

/**
 * Add an element to the front of a list
 */
List.prototype.pushFront = function (element) {
  this.dataStore.splice(0, 0, element);
  this.listSize += 1;
};

/**
 * Delete the last element of a list
 */
List.prototype.pop = function () {
  var back = this.listSize - 1;
  this.dataStore.splice(back, 1);
  this.listSize -= 1;
};

/**
 * Remove all elemenets from a list
 */
List.prototype.clear = function () {
  delete this.dataStore;
  this.dataStore = [];
  this.listSize = 0;
  this.pos = 0;
};

/**
 * Find an element in a list
 * If match, return postion of first matching element
 * Otherwise, return -1 
 */
List.prototype.find = function (element, compareFunc) {
  var idx = 0;
  var len = this.dataStore.length;

  if (!compareFunc) { // CompareFunc wasn't provided
    for (idx = 0; idx < len; ++idx) {
      if (this.dataStore[idx] === element) {
        return idx;
      }
    }
  } else { // CompareFunc was provided
    for (idx = 0; idx < len; ++idx) {
      if (compareFunc(element, this.dataStore[idx])) {
        return idx;
      }
    }
  }

  return -1;
};

/**
 * Determining if a given element is in a list
 */
List.prototype.contains = function (element, compareFunc) {
  var idx = this.find(element, compareFunc);
  if (idx > -1) {
    return true;
  }
  return false;
};

/**
 * Determining the number of elements in a list
 */
List.prototype.length = function () {
  return this.listSize;
};

/**
 * Retrieving a list's elements
 */
List.prototype.getData = function () {
  return this.dataStore;
};

/**
 * Move to the front of a list
 */
List.prototype.front = function () {
  this.pos = 0;
};

/**
 * Move to the end of a list
 */
List.prototype.end = function () {
  this.pos = this.listSize - 1;
};

/**
 * Move to the previous position
 */
List.prototype.prev = function () {
  if (this.pos > 0) {
    this.pos -= 1;
  }
};

/**
 * Move to the next position
 */
List.prototype.next = function () {
  if (this.pos < this.listSize) {
    this.pos += 1;
  }
};

/**
 * Get current position
 */
List.prototype.currPos = function () {
  return this.pos;
};

/**
 * Move to a given position
 */
List.prototype.moveTo = function (position) {
  try {
    if (position > this.listSize || position < 0) {
      throw new Error('Out of range')
    }
  } finally {
    this.pos = position;
  }
};

/**
 * Get element at current position
 */
List.prototype.getElement = function () {
  return this.dataStore[this.pos];
};

/**
 * Get element at given position
 */
List.prototype.getElementAt = function (position) {
  try {
    if (position > this.listSize || position < 0) {
      throw new Error('Out of range')
    }
  } finally {
    return this.dataStore[position];
  }
};

/**
 * Get element at given position backward
 */
List.prototype.getElementReverseAt = function (position) {
  try {
    if (position > this.listSize || position < 0) {
      throw new Error('Out of range')
    }
  } finally {
    return this.dataStore[this.listSize - position - 1];
  }
}
