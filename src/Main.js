'use strict';

var canvas = document.getElementById('canvas');
var context = this.canvas.getContext('2d');
var width = canvas.width;
var height = canvas.height;

var snakeModel = new SnakeModel(context, height, height, 20);
var snakeController = new SnakeController(snakeModel);
var snakeView = new SnakeView(snakeModel);
var boardView = new BoardView(snakeModel);
var fruitView = new FruitView(snakeModel);
var scoreView = new ScoreView(snakeModel);

snakeModel.addView(snakeView);
snakeModel.addView(boardView);
snakeModel.addView(fruitView);
snakeModel.addView(scoreView);
snakeModel.initSnake();

snakeModel.tick();
